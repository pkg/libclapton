/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>
#include <libclapton-log.h>

static void
send_debug (void)
{
  g_debug ("Debug");
  g_info ("Info");
  g_message ("Message");
  g_warning ("Warning");
  g_critical ("Critical");
  /* Don't test g_error() are those are always fatal */
}

int
main (int argc, char **argv)
{
  /* We don't want to crash while testing */
  g_log_set_always_fatal (0);

  g_print ("Redirect logs to journal\n");
  clapton_log_register ();
  send_debug ();

  g_print ("Stop redirecting logs to journal\n");
  clapton_log_unregister ();
  send_debug ();

  return 0;
}
