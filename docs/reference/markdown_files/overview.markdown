## Libclapton Overview

[Mail the maintainers](mailto:libclapton@apertis.org)

LibClapton is a library which is used for logging and getting system information.

### Brief introduction to libclapton

libclapton provides two components:

+ The ClaptonLog interface This component provides logging mechanism for applications and
  libraries. Clapton redirects log to specified file to homedirectory/Log/name.log

+ clapton system info interface ClaptonSysInfo will provide all generic system configuration data.
  If any application like Appstore want to query the system info this libraries can be used. 
  This can facilitate the user with suite name,host name,kernel version,variant name,build version etc..
