/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __CLAPTON_LOG_H__
#define __CLAPTON_LOG_H__

#include <glib.h>
#include <glib-object.h>

/**
 * SECTION:log/libclapton-log.h
 * @title: ClaptonLog
 * @short_description: Redirects the log to systemd's journal
 *
 * Clapton redirects log to the systemd journal. Just call
 * clapton_log_register() and all your GLib logs will be redirected to the
 * journal. You can then use journalctl to retrieve the logs.
 */

G_BEGIN_DECLS

void clapton_log_register (void);
void clapton_log_unregister (void);

G_END_DECLS

#endif /* __CLAPTON_LOG_H__ */
