/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */



#include "config.h"

#include <gio/gio.h>
#include <glib.h>
#include <stdio.h>
#define SD_JOURNAL_SUPPRESS_LOCATION 1
#include <systemd/sd-journal.h>
#include <syslog.h>
#include <unistd.h>

#include "libclapton-log.h"

static unsigned int
log_level_to_priority (GLogLevelFlags log_level)
{
  switch (log_level & G_LOG_LEVEL_MASK)
  {
    /* GLib's CRITICAL is one step below ERROR (it's a "critical warning"),
     * whereas syslog's CRIT is one step above ERROR (it's a "critical
     * error") */
    case G_LOG_LEVEL_ERROR:
      return LOG_CRIT;
    case G_LOG_LEVEL_CRITICAL:
      return LOG_ERR;
    case G_LOG_LEVEL_WARNING:
      return LOG_WARNING;
    case G_LOG_LEVEL_MESSAGE:
      return LOG_NOTICE;
    case G_LOG_LEVEL_INFO:
      return LOG_INFO;
    case G_LOG_LEVEL_DEBUG:
      return LOG_DEBUG;
    default:
      return LOG_DEBUG;
  }
}

static gboolean
output_to_tty (GLogLevelFlags log_level)
{
  int fd;

  /* From g_log_default_handler's doc: "stderr is used for levels
   * G_LOG_LEVEL_ERROR, G_LOG_LEVEL_CRITICAL, G_LOG_LEVEL_WARNING and
   * G_LOG_LEVEL_MESSAGE. stdout is used for the rest." */
  switch (log_level & G_LOG_LEVEL_MASK)
  {
    case G_LOG_LEVEL_ERROR:
    case G_LOG_LEVEL_CRITICAL:
    case G_LOG_LEVEL_WARNING:
    case G_LOG_LEVEL_MESSAGE:
      fd = fileno (stderr);
      break;
    case G_LOG_LEVEL_INFO:
    case G_LOG_LEVEL_DEBUG:
    default:
      fd = fileno (stdout);
      break;
  }

  return isatty (fd);
}

static void
log_cb (const gchar    *log_domain,
        GLogLevelFlags  log_level,
        const gchar    *message,
        gpointer        user_data)
{
  unsigned int priority;
  gchar *msg_with_domain = NULL;
  GApplication *app;
  const gchar *identifier = NULL;

  priority = log_level_to_priority (log_level);

  if (log_domain)
    msg_with_domain = g_strdup_printf ("[%s] %s", log_domain, message);

  /* Try to use the application identifier if available */
  app = g_application_get_default ();
  if (app)
    identifier = g_application_get_application_id (app);

  if (identifier)
    sd_journal_send ("MESSAGE=%s", msg_with_domain ? msg_with_domain : message,
                     "PRIORITY=%d", priority,
                     "SYSLOG_IDENTIFIER=%s", identifier,
                     NULL);
  else
    sd_journal_send ("MESSAGE=%s", msg_with_domain ? msg_with_domain : message,
                     "PRIORITY=%d", priority,
                     NULL);

  /* Also display the log if we are running from a console to ease debugging */
  if (output_to_tty (log_level))
    g_log_default_handler (log_domain, log_level, message, user_data);

  g_free (msg_with_domain);
}

/**
 * clapton_log_register:
 *
 * Change the default log handler so that all the logs are redirected to
 * systemd's journal. If the application is executed from a terminal, logs
 * will also be displayed to it.
 *
 * You can use clapton_log_unregister() to restore the default log handler.
 */
void
clapton_log_register (void)
{
  g_log_set_default_handler (log_cb, NULL);
}

/**
 * clapton_log_unregister:
 *
 * Restore GLib's default log handler.
 */
void
clapton_log_unregister (void)
{
  g_log_set_default_handler (g_log_default_handler, NULL);
}
